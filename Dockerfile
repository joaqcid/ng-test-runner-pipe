FROM node:10.15.3

RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
    echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list && \
    apt-get update && \
    apt-get install -y google-chrome-stable xvfb procps

#RUN npm install -g @angular/cli
RUN npm install protractor

COPY pipe.sh /
RUN chmod a+x /*.sh

ENTRYPOINT ["/pipe.sh"]